package allData.dao;



import allData.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by discovery on 23.07.2015.
 */
public interface UserDao extends JpaRepository<Users, Integer> {
}
