package allData.dao;



import allData.models.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by discovery on 23.07.2015.
 */
public interface CommentDao extends JpaRepository<Comments, Integer> {
}
