package allData.dao;


import allData.models.Posts;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by discovery on 23.07.2015.
 */
public interface PostsDao extends JpaRepository<Posts, Integer> {

}
