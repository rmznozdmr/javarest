package allData.dao;



import allData.models.Categories;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by discovery on 23.07.2015.
 */
public interface CategoriesDao extends JpaRepository<Categories, Integer> {

}
