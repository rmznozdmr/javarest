package allData.controllers;


import allData.models.Categories;
import allData.services.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import java.util.List;


/**
 * Created by discovery on 23.07.2015.
 */
@Controller
@RequestMapping("/categories")
public class CategoriesController  {

    @Autowired
    private CategoriesService servicex;

    public CategoriesController(){
        servicex= new CategoriesService();
    }



    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody Categories entity) {
        servicex.create(entity);

    }

    @RequestMapping(value="{id}", method = RequestMethod.PUT)
    public void edit(@PathVariable Integer id,@RequestBody Categories entity) {
        servicex.update(entity);
    }

    @RequestMapping(value="{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Integer id) {
        servicex.delete(id);
    }

    @RequestMapping(value="{id}", method = RequestMethod.GET)
    public Categories find(@PathVariable Integer id) {
        return servicex.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)

    public List<Categories> findAll() {
        return servicex.findAll();
    }

    /*@RequestMapping(value="{from}/{to}", method = RequestMethod.PUT)
    public List<Categories> findRange(@PathVariable Integer from, @PathVariable Integer to) {
        return service.findRange(new int[]{from, to});
    }*/





}

