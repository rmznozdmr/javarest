package allData.controllers;


import allData.models.Users;
import allData.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import java.util.List;

/**
 * Created by discovery on 23.07.2015.
 */
@Controller
@RequestMapping("/users")
public class UsersController  {

    @Autowired
    private UsersService servicex;

    public UsersController(){
    }



    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody Users entity) {
        servicex.create(entity);

    }

    @RequestMapping(value="{id}", method = RequestMethod.PUT)
    public void edit(@PathVariable Integer id,@RequestBody Users entity) {
        servicex.update(entity);
    }

    @RequestMapping(value="{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Integer id) {
        servicex.delete(id);
    }

    @RequestMapping(value="{id}", method = RequestMethod.GET)
    public Users find(@PathVariable Integer id) {
        return servicex.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)

    public List<Users> findAll() {
        return servicex.findAll();
    }

}
