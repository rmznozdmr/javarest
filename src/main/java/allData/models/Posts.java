/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allData.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author discovery
 */
@Entity
@Table(name = "POSTS")

public class Posts implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "POST_ID")
    private Integer postId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "POST_CONTENT")
    private String postContent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "POST_DATE")
    @Temporal(TemporalType.DATE)
    private Date postDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "POST_BY")
    private int postBy;
    @JoinColumn(name = "POST_CAT", referencedColumnName = "CAT_ID")
    @ManyToOne(optional = false)
    private Categories postCat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "commentPost")
    private Collection<Comments> commentsCollection;

    public Posts() {
    }

    public Posts(Integer postId) {
        this.postId = postId;
    }

    public Posts(Integer postId, String postContent, Date postDate, int postBy) {
        this.postId = postId;
        this.postContent = postContent;
        this.postDate = postDate;
        this.postBy = postBy;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public int getPostBy() {
        return postBy;
    }

    public void setPostBy(int postBy) {
        this.postBy = postBy;
    }

    public Categories getPostCat() {
        return postCat;
    }

    public void setPostCat(Categories postCat) {
        this.postCat = postCat;
    }

    /*@XmlTransient
    public Collection<Comments> getCommentCollection() {
        return commentsCollection;
    }

    public void setCommentCollection(Collection<Comments> commentsCollection) {
        this.commentsCollection = commentsCollection;
    }*/



}
