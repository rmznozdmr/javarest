/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allData.models;

import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 *
 * @author discovery
 */
@Entity
@Table(name = "COMMENTS")

public class Comments {
    @Id
    @GeneratedValue
    @Column(name = "COMMENT_ID")
    private Integer commentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "COMMENTS")
    private String comment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMMENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date commentDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMMENT_BY")
    private int commentBy;
    @JoinColumn(name = "COMMENT_POST", referencedColumnName = "POST_ID")
    @ManyToOne(optional = false)
    private Posts commentPost;

    public Comments() {
    }

    public Comments(Integer commentId) {
        this.commentId = commentId;
    }

    public Comments(Integer commentId, String comment, Date commentDate, int commentBy) {
        this.commentId = commentId;
        this.comment = comment;
        this.commentDate = commentDate;
        this.commentBy = commentBy;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public int getCommentBy() {
        return commentBy;
    }

    public void setCommentBy(int commentBy) {
        this.commentBy = commentBy;
    }

    public Posts getCommentPost() {
        return commentPost;
    }

    public void setCommentPost(Posts commentPost) {
        this.commentPost = commentPost;
    }



}
