package allData.services;



import allData.dao.UserDao;
import allData.models.Users;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by discovery on 23.07.2015.
 */
@Component
public class UsersService  implements servisinterface<Users> {
    @Resource
    private UserDao mydao;

    @Override
    @Transactional
    public Users create(Users shop) {
        Users createcat=shop;

        return mydao.save(shop);
    }

    @Override
    @Transactional
    public Users delete(int id) {

        Users deleted=mydao.findOne(id);

        mydao.delete(mydao.findOne(id));
        return deleted;
    }

    @Override
    @Transactional
    public List<Users> findAll() {
        return mydao.findAll();
    }

    @Override
    @Transactional
    public Users update(Users shop) {
        Users updatecat=mydao.findOne(shop.getUserId());
        updatecat.setUserName(shop.getUserName());
        updatecat.setUserEmail(shop.getUserEmail());
        updatecat.setUserPass(shop.getUserPass());



        return updatecat;
    }

    @Override
    @Transactional
    public Users findById(int id) {
        return mydao.findOne(id);

    }
}
