package allData.services;


import allData.dao.PostsDao;
import allData.models.Posts;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by discovery on 23.07.2015.
 */
@Component
public class PostsService implements servisinterface<Posts> {

    @Resource
    private PostsDao mydao;

    @Override
    @Transactional
    public Posts create(Posts shop) {
        Posts createcat=shop;

        return mydao.save(shop);
    }

    @Override
    @Transactional
    public Posts delete(int id) {

        Posts deleted=mydao.findOne(id);

        mydao.delete(mydao.findOne(id));
        return deleted;
    }

    @Override
    @Transactional
    public List<Posts> findAll() {
        return mydao.findAll();
    }

    @Override
    @Transactional
    public Posts update(Posts shop) {
        Posts updatecat=mydao.findOne(shop.getPostId());
        //updatecat.setCommentsCollection(shop.getCommentCollection());
        updatecat.setPostCat(shop.getPostCat());
        updatecat.setPostContent(shop.getPostContent());


        return updatecat;
    }

    @Override
    @Transactional
    public Posts findById(int id) {
        return mydao.findOne(id);

    }
}
