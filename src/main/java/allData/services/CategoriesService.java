package allData.services;



import allData.dao.CategoriesDao;
import allData.models.Categories;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by discovery on 23.07.2015.
 */
@Component
public class CategoriesService implements servisinterface<Categories> {
    @Resource
    private CategoriesDao mydao;

    @Override
    @Transactional
    public Categories create(Categories shop) {
        Categories createcat=shop;

        return mydao.save(shop);
    }

    @Override
    @Transactional
    public Categories delete(int id) {

        Categories deleted=mydao.findOne(id);

        mydao.delete(mydao.findOne(id));
        return deleted;
    }

    @Override
    @Transactional
    public List<Categories> findAll() {
        return mydao.findAll();
    }

    @Override
    @Transactional
    public Categories update(Categories shop) {
        Categories updatecat=mydao.findOne(shop.getCatId());
        updatecat.setCatDescription(shop.getCatDescription());
        updatecat.setCatName(shop.getCatName());
        //updatecat.setPostsCollection(shop.getPostsCollection());

        return updatecat;
    }

    @Override
    @Transactional
    public Categories findById(int id) {
        return mydao.findOne(id);

    }
}
