package allData.services;

import java.util.List;

/**
 * Created by discovery on 23.07.2015.
 */
public interface servisinterface<T> {
    public T create(T shop);
    public T delete(int id) ;
    public List<T> findAll();
    public T update(T shop) ;
    public T findById(int id);
}
