package allData.services;


import allData.dao.CommentDao;
import allData.models.Comments;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by discovery on 23.07.2015.
 */
@Component
public class CommentsService implements servisinterface<Comments> {


    @Resource
    private CommentDao mydao;

    @Override
    @Transactional
    public Comments create(Comments shop) {
        Comments createcat=shop;

        return mydao.save(shop);
    }

    @Override
    @Transactional
    public Comments delete(int id) {

        Comments deleted=mydao.findOne(id);

        mydao.delete(mydao.findOne(id));
        return deleted;
    }

    @Override
    @Transactional
    public List<Comments> findAll() {
        return mydao.findAll();
    }

    @Override
    @Transactional
    public Comments update(Comments shop) {
        Comments updatecat=mydao.findOne(shop.getCommentId());
        updatecat.setComment(shop.getComment());
        updatecat.setCommentDate(shop.getCommentDate());
        updatecat.setCommentPost(shop.getCommentPost());


        return updatecat;
    }

    @Override
    @Transactional
    public Comments findById(int id) {
        return mydao.findOne(id);

    }
}
